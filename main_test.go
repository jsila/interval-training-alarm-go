package main

import (
	"testing"
	"time"

	"github.com/cheekybits/is"
)

func d(s int) time.Duration {
	return time.Duration(s) * time.Second
}

func TestCreateIntervalSequence(t *testing.T) {
	tc := []struct {
		countdown, duration, pause string
		repeatN                    int
		seq                        []time.Duration
		progress                   []int
	}{
		{"0s", "2s", "3s", 0, []time.Duration{}, []int{}},
		{"2s", "2s", "3s", 2, []time.Duration{d(0), d(1), d(2), d(4), d(7), d(9)}, []int{PREPARE, PREPARE, START, REST, WORKOUT, END}},
	}

	is := is.New(t)
	for _, c := range tc {
		cd, _ := time.ParseDuration(c.countdown)
		d, _ := time.ParseDuration(c.duration)
		p, _ := time.ParseDuration(c.pause)

		seq, prog := CreateIntervalSequence(cd, d, p, c.repeatN)
		is.Equal(prog, c.progress)
		is.Equal(seq, c.seq)
	}
}
