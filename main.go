package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"time"
)

const (
	PREPARE = iota
	START
	REST
	WORKOUT
	END
)

var Sounds = map[int]func(){
	PREPARE: PlayPrepareSound,
	START:   PlayWorkoutSound,
	REST:    PlayRestSound,
	WORKOUT: PlayWorkoutSound,
	END:     PlayEndSound,
}

func main() {
	countdown := flag.Duration("countdown", time.Duration(5*time.Second), "time to prepare")

	flag.Parse()

	if flag.NArg() != 3 {
		fmt.Println("Specify an workout interval, rest interval and nmber of repeats")
		os.Exit(1)
	}

	workoutI, err := time.ParseDuration(flag.Arg(0))
	if err != nil {
		fmt.Println("Specify workout interval in valid format (such as '300ms', '-1.5h' or '2h45m'. Valid time units are 'ns', 'us' (or 'µs'), 'ms', 's', 'm', 'h')")
		os.Exit(1)
	}

	restI, err := time.ParseDuration(flag.Arg(1))
	if err != nil {
		fmt.Println("Specify rest interval in valid format (such as '300ms', '-1.5h' or '2h45m'. Valid time units are 'ns', 'us' (or 'µs'), 'ms', 's', 'm', 'h')")
		os.Exit(1)
	}
	repeatN, err := strconv.Atoi(flag.Arg(2))
	if err != nil {
		fmt.Println("Specify valid number of repeats")
		os.Exit(1)
	}

	seq, progress := CreateIntervalSequence(*countdown, workoutI, restI, repeatN)

	DisplayIntervals(seq, progress)
}

func CreateIntervalSequence(c time.Duration, d time.Duration, r time.Duration, repeatN int) ([]time.Duration, []int) {
	seq := []time.Duration{}
	progress := []int{}

	if repeatN == 0 {
		return seq, progress
	}
	seq = append(seq, time.Duration(0))
	for i := int(c.Seconds()); i > 0; i-- {
		seq = append(seq, time.Duration(time.Second))
		progress = append(progress, PREPARE)
	}

	for i := 0; i < repeatN-1; i++ {
		seq = append(seq, d, r)
		startType := WORKOUT
		if i == 0 {
			startType = START
		}
		progress = append(progress, startType, REST)
	}

	seq = append(seq, d)
	progress = append(progress, WORKOUT, END)

	for i := 1; i < len(seq); i++ {
		seq[i] = seq[i-1] + seq[i]
	}

	return seq, progress
}

func DisplayIntervals(seq []time.Duration, progress []int) {
	i := 0
	for d := time.Duration(0); i < len(seq); d += time.Second {
		if seq[i] == d {
			go Sounds[progress[i]]()
			i++
		}
		<-time.After(time.Second)
	}
}

func PlayPrepareSound() {
	exec.Command("paplay", "/usr/share/sounds/gnome/default/alerts/sonar.ogg").Run()
}
func PlayStartSound() {
	exec.Command("paplay", "/usr/share/sounds/freedesktop/stereo/phone-incoming-call.oga").Run()
}
func PlayWorkoutSound() {
	exec.Command("paplay", "/usr/share/sounds/freedesktop/stereo/phone-outgoing-calling.oga").Run()
}
func PlayRestSound() {
	exec.Command("paplay", "/usr/share/sounds/freedesktop/stereo/complete.oga").Run()
}
func PlayEndSound() {
	exec.Command("paplay", "/usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga").Run()
}
