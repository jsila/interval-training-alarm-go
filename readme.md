# Interval training alarm

CLI app that helps you perform an interval training by being your stopwatch.

It lets you specify length of an intervals for workout and rest periods (in `time.Duration` format) and number of repeats. Countdown time is also included and defaults to 5 seconds (you can modify this time, even down to `0s`).

Each time event is equipped with a different sound:
 - countdown
 - start of an training (first workout)
 - start of a rest
 - start of a workout
 - end of a training (when last workout ends)

Currently only same-length intervals and same-length rest times are supported.

## Usage

Say you want to do 6x10s push-ups with 5s rest between intervals, starting immediately.

```bash
# ./interval-training-alarm [--countdown=<duration>] <interval duration> <rest duration> <repeats>
./interval-training-alarm --countdown=0s 10s 5s 6
```